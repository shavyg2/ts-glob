# ts-glob

## The typescript file glob updater


This package is very simple. Inside your package.json set the field ts.

```javascript

    {
        "ts":{
            config:"tsconfig.json",
            files:[
                "lib/**/*.ts",
                "test/**/*.ts"
            ]
        }
    }

    //

    {
        "ts":[{
            config:"tsconfig.json",
            files:[
                "lib/**/*.ts",
                "test/**/*.ts"
            ]
        }]
    }

```

Once this is configured you can add any amount of files to your typescript project by running the ```ts-glob``` or ```tsglob``` command.



# Changelog

- 0.0.8
    - Updated to use relative path instead of absolute path when generating files.