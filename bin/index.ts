#!/usr/bin/env node
import "source-map-support/register";
import { TsfileGenerator } from '../lib/TsFileGenerator';

const unhandledRejection = require("unhandled-rejection");
 
// Assuming `loggingServer` is some kind of logging API... 
 
let rejectionEmitter = unhandledRejection({
    timeout: 20
});
 
rejectionEmitter.on("unhandledRejection", (error, promise) => {
    console.log(error);
});
 
rejectionEmitter.on("rejectionHandled", (error, promise) => {
    console.log(error);
})

async function Run(){
    console.log(`Running ts-glob Generator`);
    try{
        await TsfileGenerator.Run()    
    }catch(e){
        console.log("Failed to Generate tsconfig.files")
        throw e;
    }
}

Run();