#!/usr/bin/env node
"use strict";
var tslib_1 = require("tslib");
require("source-map-support/register");
var TsFileGenerator_1 = require("../lib/TsFileGenerator");
var unhandledRejection = require("unhandled-rejection");
// Assuming `loggingServer` is some kind of logging API... 
var rejectionEmitter = unhandledRejection({
    timeout: 20
});
rejectionEmitter.on("unhandledRejection", function (error, promise) {
    console.log(error);
});
rejectionEmitter.on("rejectionHandled", function (error, promise) {
    console.log(error);
});
function Run() {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var e_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log("Running ts-glob Generator");
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, TsFileGenerator_1.TsfileGenerator.Run()];
                case 2:
                    _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    e_1 = _a.sent();
                    console.log("Failed to Generate tsconfig.files");
                    throw e_1;
                case 4: return [2 /*return*/];
            }
        });
    });
}
Run();
//# sourceMappingURL=index.js.map