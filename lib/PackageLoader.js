"use strict";
var path = require("path");
var app_1 = require("../log/app");
var log = app_1.AppLog.extend("package-loader", {
    tags: ["package", "loader"]
});
var PackageLoader = (function () {
    function PackageLoader() {
        log.info("created PackageLoader");
    }
    Object.defineProperty(PackageLoader.prototype, "filename", {
        get: function () {
            return path.resolve(process.cwd(), "package.json");
        },
        enumerable: true,
        configurable: true
    });
    PackageLoader.prototype.load = function () {
        log.info("loading package.json file");
        try {
            var file = this.filename;
            var PackageJSON = require(file);
            return PackageJSON.ts;
        }
        catch (e) {
            var msg = "No Package.json file found in working directory.";
            msg = msg + ("\nCWD: " + process.cwd());
            log.error(msg);
            throw new Error(msg);
        }
    };
    return PackageLoader;
}());
exports.PackageLoader = PackageLoader;
//# sourceMappingURL=PackageLoader.js.map