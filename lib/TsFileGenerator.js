"use strict";
var tslib_1 = require("tslib");
var app_1 = require("../log/app");
var PackageLoader_1 = require("./PackageLoader");
var glob = require("glob");
var promise_lib_1 = require("promise-lib");
var path = require("path");
var ConfigWriter_1 = require("./ConfigWriter");
var Regex = require("utils-regex-from-string");
var log = app_1.AppLog.extend("generator", {
    tags: ["generator"]
});
var TsfileGenerator = (function () {
    function TsfileGenerator(loader, writer) {
        this.loader = loader;
        this.writer = writer;
        log.info("created generator");
    }
    TsfileGenerator.prototype.run = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var files;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadTsFiles()];
                    case 1:
                        files = _a.sent();
                        return [4 /*yield*/, this.write(files)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TsfileGenerator.prototype.loadTsFiles = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            var config, _config, _files;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        config = this.loader.load();
                        if (!(config instanceof Array)) return [3 /*break*/, 2];
                        _config = new promise_lib_1.PArray(config);
                        return [4 /*yield*/, _config.map(function (file) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                return tslib_1.__generator(this, function (_a) {
                                    return [2 /*return*/, this.loadTsFileConfig(file)];
                                });
                            }); })];
                    case 1:
                        _files = _a.sent();
                        return [2 /*return*/, _files.data.reduce(function (prev, next) {
                                return prev.concat(next);
                            })];
                    case 2: return [2 /*return*/, this.loadTsFileConfig(config)];
                }
            });
        });
    };
    TsfileGenerator.prototype.globber = function (glob) {
        return path.relative(process.cwd(), glob);
    };
    TsfileGenerator.prototype.loadTsFileConfig = function (config) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            var files, each, rgex;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        files = new promise_lib_1.PArray(config.files);
                        return [4 /*yield*/, files.map(function (file) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
                                var glob_path, _a, err, files;
                                return tslib_1.__generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0:
                                            glob_path = this.globber(file);
                                            return [4 /*yield*/, promise_lib_1.CallBack.Call(glob, glob_path)];
                                        case 1:
                                            _a = _b.sent(), err = _a[0], files = _a[1];
                                            return [2 /*return*/, files];
                                    }
                                });
                            }); })];
                    case 1:
                        files = _a.sent();
                        config.exclude = config.exclude || "/^$/";
                        each = files.data;
                        rgex = Regex(config.exclude);
                        return [2 /*return*/, each.reduce(function (prev, next) {
                                prev = prev || [];
                                prev = prev.concat(next);
                                return prev;
                            }).filter(function (file) {
                                return !rgex.test(file);
                            })];
                }
            });
        });
    };
    TsfileGenerator.prototype.write = function (files) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.writer.write(files)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TsfileGenerator.Create = function (file) {
        log.info("Creating tsFileGenerator from static method");
        var pl = new PackageLoader_1.PackageLoader();
        log.info("Config file used " + pl.filename);
        return new TsfileGenerator(pl, new ConfigWriter_1.ConfigWriter(file));
    };
    TsfileGenerator.Run = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var pl, config, _i, config_1, c;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        debugger;
                        pl = new PackageLoader_1.PackageLoader();
                        config = pl.load();
                        if (!(config instanceof Array)) return [3 /*break*/, 5];
                        _i = 0, config_1 = config;
                        _a.label = 1;
                    case 1:
                        if (!(_i < config_1.length)) return [3 /*break*/, 4];
                        c = config_1[_i];
                        return [4 /*yield*/, this.RunSingle(pl, c)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        _i++;
                        return [3 /*break*/, 1];
                    case 4: return [3 /*break*/, 7];
                    case 5: return [4 /*yield*/, this.RunSingle(pl, config)];
                    case 6:
                        _a.sent();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    TsfileGenerator.RunSingle = function (pl, config) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var tsg;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        debugger;
                        tsg = new TsfileGenerator(pl, new ConfigWriter_1.ConfigWriter(config.config));
                        return [4 /*yield*/, tsg.run()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return TsfileGenerator;
}());
exports.TsfileGenerator = TsfileGenerator;
//# sourceMappingURL=TsFileGenerator.js.map