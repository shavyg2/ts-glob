export interface PackageJsonTSConfig {
    config: string;
    files: string[];
    exclude?: string;
}
export interface PackageJsonFile {
    ts: PackageJsonTSConfig | PackageJsonTSConfig[];
}
export declare class PackageLoader {
    constructor();
    readonly filename: string;
    load(): PackageJsonTSConfig | PackageJsonTSConfig[];
}
