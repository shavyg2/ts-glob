import { AppLog } from "../log/app";
import { PackageLoader, PackageJsonTSConfig } from './PackageLoader';
import * as glob from "glob";
import { CallBack, PArray } from "promise-lib";
import * as path from 'path';
import { ConfigWriter } from './ConfigWriter';
const Regex = require("utils-regex-from-string");


const log = AppLog.extend("generator", {
    tags: ["generator"]
})

export class TsfileGenerator {

    constructor(protected loader: PackageLoader, protected writer: ConfigWriter) {
        log.info("created generator");
    }


    async run() {
        let files = await this.loadTsFiles();
        await this.write(files);
    }

    async loadTsFiles() {
        let config = this.loader.load();

        if (config instanceof Array) {
            let _config = new PArray(config);

            let _files = await _config.map(async (file) => {
                return this.loadTsFileConfig(file);
            })

            return _files.data.reduce((prev, next) => {
                return prev.concat(next);
            });
        } else {
            return this.loadTsFileConfig(config);
        }
    }


    globber(glob: string) {
        return path.relative(process.cwd(), glob);
    }


    async loadTsFileConfig(config: PackageJsonTSConfig) {
        let files = new PArray(config.files);

        files = await files.map(async (file) => {
            let glob_path = this.globber(file);
            let [err, files] = await CallBack.Call(glob, glob_path);

            return files;

        })

        config.exclude = config.exclude || "/^$/";

        let each = files.data as any as string[][];

        let rgex = <RegExp>Regex(config.exclude);
        return each.reduce((prev, next) => {
            prev = prev || [];
            prev = prev.concat(next);
            return prev;
        }).filter(file => {
            return !rgex.test(file)
        })
    }


    async write(files: string[]) {
        await this.writer.write(files);
    }


    static Create(file: string) {
        log.info("Creating tsFileGenerator from static method");
        let pl = new PackageLoader();
        log.info(`Config file used ${pl.filename}`);

        return new TsfileGenerator(pl, new ConfigWriter(file));
    }

    static async Run() {
        debugger;
        let pl = new PackageLoader();

        let config = pl.load();

        if (config instanceof Array) {
            for (let c of config) {
                await this.RunSingle(pl, c);
            }
        } else {
            await this.RunSingle(pl,config);
        }
    }

    static async RunSingle(pl: PackageLoader, config: PackageJsonTSConfig) {
        debugger;
        let tsg = new TsfileGenerator(pl, new ConfigWriter(config.config));
        await tsg.run()
    }

}