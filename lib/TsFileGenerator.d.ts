/// <reference types="core-js" />
import { PackageLoader, PackageJsonTSConfig } from './PackageLoader';
import { ConfigWriter } from './ConfigWriter';
export declare class TsfileGenerator {
    protected loader: PackageLoader;
    protected writer: ConfigWriter;
    constructor(loader: PackageLoader, writer: ConfigWriter);
    run(): Promise<void>;
    loadTsFiles(): Promise<string[]>;
    globber(glob: string): string;
    loadTsFileConfig(config: PackageJsonTSConfig): Promise<string[]>;
    write(files: string[]): Promise<void>;
    static Create(file: string): TsfileGenerator;
    static Run(): Promise<void>;
    static RunSingle(pl: PackageLoader, config: PackageJsonTSConfig): Promise<void>;
}
