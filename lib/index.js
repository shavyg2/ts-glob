"use strict";
var tslib_1 = require("tslib");
var TsFileGenerator_1 = require("./TsFileGenerator");
var PackageLoader_1 = require("./PackageLoader");
var ConfigWriter_1 = require("./ConfigWriter");
var TsfileGeneratorRuntime = (function (_super) {
    tslib_1.__extends(TsfileGeneratorRuntime, _super);
    function TsfileGeneratorRuntime() {
        var _this = this;
        var pl = new PackageLoader_1.PackageLoader();
        var writer = new ConfigWriter_1.ConfigWriter(pl.filename);
        _this = _super.call(this, pl, writer) || this;
        return _this;
    }
    return TsfileGeneratorRuntime;
}(TsFileGenerator_1.TsfileGenerator));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TsfileGeneratorRuntime;
//# sourceMappingURL=index.js.map