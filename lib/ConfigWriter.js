"use strict";
var tslib_1 = require("tslib");
var promise_lib_1 = require("promise-lib");
var fs = require("fs");
var app_1 = require("../log/app");
var path = require("path");
var log = app_1.AppLog.extend("config-writer", {
    tags: ["config", "writer"]
});
var ConfigWriter = (function () {
    function ConfigWriter(filename) {
        this.filename = filename;
        if (!path.isAbsolute(filename)) {
            this.filename = path.resolve(process.cwd(), filename);
        }
        log.info("created ConfigWriter instance");
    }
    ConfigWriter.prototype.write = function (files) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var config, err;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        log.info("loading " + this.filename);
                        try {
                            config = require(this.filename);
                        }
                        catch (e) {
                            log.error(e.message);
                            throw e;
                        }
                        config.files = files;
                        return [4 /*yield*/, promise_lib_1.CallBack.Call(fs.writeFile, this.filename, JSON.stringify(config, null, 2))];
                    case 1:
                        err = (_a.sent())[0];
                        if (err) {
                            log.error("Error writing to file " + this.filename);
                            throw err;
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return ConfigWriter;
}());
exports.ConfigWriter = ConfigWriter;
//# sourceMappingURL=ConfigWriter.js.map