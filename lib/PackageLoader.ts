import * as path from "path";

import {AppLog} from "../log/app";

const log = AppLog.extend("package-loader",{
    tags:["package","loader"]
})

export interface PackageJsonTSConfig {
    config:string,
    files:string[],
    exclude?:string
}

export interface PackageJsonFile{
    ts:PackageJsonTSConfig|PackageJsonTSConfig[]
}

export class PackageLoader{


    constructor(){
        log.info("created PackageLoader")
    }


    get filename(){
        return path.resolve(process.cwd(),"package.json");
    }

    load(){
        log.info("loading package.json file");
        try{
            let file = this.filename;
            let PackageJSON = require(file) as PackageJsonFile
            return PackageJSON.ts;
        }catch(e){
            let msg = "No Package.json file found in working directory."
            msg = msg+`\nCWD: ${process.cwd()}`
            log.error(msg);
            throw new Error(msg);
        }
    }
}