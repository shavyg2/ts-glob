import { CallBack } from 'promise-lib';
import * as fs from "fs";
import { AppLog } from '../log/app';
import * as path from 'path';

const log = AppLog.extend("config-writer", {
    tags: ["config", "writer"]
})
export class ConfigWriter {

    constructor(protected filename: string) {
        if(!path.isAbsolute(filename)){
            this.filename = path.resolve(process.cwd(),filename);
        }
        log.info("created ConfigWriter instance");
    }

    async write(files: string[]) {
        log.info(`loading ${this.filename}`)

        let config: any;
        try {
            config = require(this.filename);
        } catch (e) {
            log.error((e as Error).message);
            throw e;
        }

        config.files = files;
        let [err] = await CallBack.Call(fs.writeFile, this.filename, JSON.stringify(config, null, 2));
        if (err) {
            log.error(`Error writing to file ${this.filename}`);
            throw err;
        }
    }
}