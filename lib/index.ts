import { TsfileGenerator } from './TsFileGenerator';
import { PackageLoader } from './PackageLoader';
import { ConfigWriter } from './ConfigWriter';
export default class TsfileGeneratorRuntime extends TsfileGenerator{
    constructor(){
        let pl = new PackageLoader();
        let writer = new ConfigWriter(pl.filename);
        super(pl,writer);
    }
}