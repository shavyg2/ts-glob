/// <reference types="core-js" />
export declare class ConfigWriter {
    protected filename: string;
    constructor(filename: string);
    write(files: string[]): Promise<void>;
}
