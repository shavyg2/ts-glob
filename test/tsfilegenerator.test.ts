import * as sinon from "sinon";
import * as chai from "chai";
import { TsfileGenerator } from '../lib/TsFileGenerator';
import { PackageLoader } from '../lib/PackageLoader';
import { ConfigWriter } from '../lib/ConfigWriter';

const expect = chai.expect;



describe("TsFileGenerator Test",function(){


    let tsg:TsfileGenerator;
    let pl:PackageLoader;
    let writer:ConfigWriter;
    before(async function(){
        debugger;
        pl = new PackageLoader;
        
        tsg = new TsfileGenerator(pl,writer);
    })


    it("should",async function(){
        let config = pl.load();

        if(! (config instanceof Array)){
            let files = await tsg.loadTsFileConfig(config);
            expect(files).to.be.instanceOf(Array);
            console.log(files)
        }else{
            throw new Error("Failed to load config");
        }
    })


    after(async function(){
        
    })
})