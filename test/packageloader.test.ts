import * as sinon from "sinon";
import * as chai from "chai";
import { PackageLoader } from '../lib/PackageLoader';

const expect = chai.expect;



describe("PackageLoader Test",function(){

    let pl:PackageLoader;

    before(async function(){
        pl = new PackageLoader();
    })


    it("should be able to load config files",async function(){
        let config = pl.load();

        if(!(config instanceof Array)){
            expect(config.config).to.exist;
            expect(config.files).instanceof(Array);
            expect(config.files).to.have.lengthOf(4);
        }else{
            throw new Error("Failed to read config file");
        }
    })


    after(async function(){
        
    })
})