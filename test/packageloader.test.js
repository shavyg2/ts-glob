"use strict";
var tslib_1 = require("tslib");
var chai = require("chai");
var PackageLoader_1 = require("../lib/PackageLoader");
var expect = chai.expect;
describe("PackageLoader Test", function () {
    var pl;
    before(function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                pl = new PackageLoader_1.PackageLoader();
                return [2 /*return*/];
            });
        });
    });
    it("should be able to load config files", function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var config;
            return tslib_1.__generator(this, function (_a) {
                config = pl.load();
                if (!(config instanceof Array)) {
                    expect(config.config).to.exist;
                    expect(config.files).instanceof(Array);
                    expect(config.files).to.have.lengthOf(4);
                }
                else {
                    throw new Error("Failed to read config file");
                }
                return [2 /*return*/];
            });
        });
    });
    after(function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    });
});
//# sourceMappingURL=packageloader.test.js.map