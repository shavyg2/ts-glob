"use strict";
var tslib_1 = require("tslib");
var chai = require("chai");
var TsFileGenerator_1 = require("../lib/TsFileGenerator");
var PackageLoader_1 = require("../lib/PackageLoader");
var expect = chai.expect;
describe("TsFileGenerator Test", function () {
    var tsg;
    var pl;
    var writer;
    before(function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                debugger;
                pl = new PackageLoader_1.PackageLoader;
                tsg = new TsFileGenerator_1.TsfileGenerator(pl, writer);
                return [2 /*return*/];
            });
        });
    });
    it("should", function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var config, files;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        config = pl.load();
                        if (!!(config instanceof Array)) return [3 /*break*/, 2];
                        return [4 /*yield*/, tsg.loadTsFileConfig(config)];
                    case 1:
                        files = _a.sent();
                        expect(files).to.be.instanceOf(Array);
                        console.log(files);
                        return [3 /*break*/, 3];
                    case 2: throw new Error("Failed to load config");
                    case 3: return [2 /*return*/];
                }
            });
        });
    });
    after(function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    });
});
//# sourceMappingURL=tsfilegenerator.test.js.map