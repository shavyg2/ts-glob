

import LimberJack from "limberjack";
import {ILimberJack} from "limberjack/lib/LimberJack";
import * as path from 'path';
import {LOGLEVEL} from "limberjack";


let file = path.resolve(process.env.TMP,"ts-glob.log");

export const AppLog = LimberJack("app",{
    file:file,
    tags:["app"],
    level:LOGLEVEL.INFO
})




import "./start";