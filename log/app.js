"use strict";
var limberjack_1 = require("limberjack");
var path = require("path");
var limberjack_2 = require("limberjack");
var file = path.resolve(process.env.TMP, "ts-glob.log");
exports.AppLog = limberjack_1.default("app", {
    file: file,
    tags: ["app"],
    level: limberjack_2.LOGLEVEL.INFO
});
require("./start");
//# sourceMappingURL=app.js.map