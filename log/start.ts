import { AppLog } from './app';
import { ILimberJack } from 'limberjack/lib/Limberjack';

export const StartLog =  AppLog.extend("start",{
    tags:["start"]
})


StartLog.info("*******************Starting**********************");